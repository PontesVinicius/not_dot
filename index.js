const express = require('express');
const cronJob = require('cron').CronJob;
const Nightmare = require('nightmare');
const port = process.env.PORT || 3000;

const nightmare = Nightmare({show: true});
const app = express();

const pontoChegada = () => {
    nightmare
      .goto('http://portaldoconsultor.altran.com.br/login')
      .type('#user_session_email', 'jorgevinicius.silva@altran.com.br')
      .type('#user_session_password', '1111')
      .click('.pull-right')
      .wait(2000)
      .evaluate(() => {
        document.querySelector('#begin_period_one').value = '10:22';
        document.querySelector('#click_id').value = 'begin_period_one';
        document.querySelector('#time_sheet_begin_period_one').value = '10:22';
        document.querySelector('form.edit_time_sheet').submit();
      })
      .wait(5000)
      .end()
      .then((data) => {console.log(data)})
      .catch(error => {
        console.error('Bater ponto chegada: ', error)
      })
  }



const pontoAlmoco = () => {
    nightmare
      .goto('http://portaldoconsultor.altran.com.br/login')
      .type('#user_session_email', 'jorgevinicius.silva@altran.com.br')
      .type('#user_session_password', '1111')
      .click('.pull-right')
      .wait(2000)
      .evaluate(() => {
        document.querySelector('#end_period_one').value = '13:25';
        document.querySelector('#click_id').value = 'end_period_one';
        document.querySelector('#time_sheet_end_period_one').value = '13:25';
        document.querySelector('form.edit_time_sheet').submit();
      })
      .wait(5000)
      .end()
      .then((data) => {console.log(data)})
      .catch(error => {
        console.error('Bater ponto almoço: ', error)
      })
  }

  const pontoVoltaAlmoco = () => {
    nightmare
      .goto('http://portaldoconsultor.altran.com.br/login')
      .type('#user_session_email', 'jorgevinicius.silva@altran.com.br')
      .type('#user_session_password', '1111')
      .click('.pull-right')
      .wait(2000)
      .evaluate(() => {
        document.querySelector('#begin_period_two').value = '14:26';
        document.querySelector('#click_id').value = 'begin_period_two';
        document.querySelector('#time_sheet_begin_period_two').value = '14:26';
        document.querySelector('form.edit_time_sheet').submit();
      })
      .wait(5000)
      .end()
      .then((data) => {console.log(data)})
      .catch(error => {
        console.error('Bater ponto volta almoço: ', error)
      })
  }

  const pontoCasa = () => {
    nightmare
      .goto('http://portaldoconsultor.altran.com.br/login')
      .type('#user_session_email', 'jorgevinicius.silva@altran.com.br')
      .type('#user_session_password', '1111')
      .click('.pull-right')
      .wait(2000)
      .evaluate(() => {
        document.querySelector('#end_period_two').value = '19:51';
        document.querySelector('#click_id').value = 'end_period_two';
        document.querySelector('#time_sheet_end_period_two').value = '19:51';
        document.querySelector('form.edit_time_sheet').submit();
      })
      .wait(5000)
      .end()
      .then((data) => {console.log(data)})
      .catch(error => {
        console.error('Bater ponto casa: ', error)
      })
  }

  minutosEntrada = 0;
  minutosAlmoco = 0;
  minutosVoltaAlmoco = 0;
  minutosSaida = 0;
const generateRandoms = () => {
    minutosEntrada = Math.ceil(Math.random() * (9 - 1)) + 50;
    minutosAlmoco = Math.ceil(Math.random() * (9 - 1));
    minutosVoltaAlmoco = Math.ceil(Math.random() * (9 - 1)) + 5;
    minutosSaida = Math.ceil(Math.random() * (9 - 1)) + 40;
}

const randomToday = new cronJob(`1 8 * * 1,2,3,4,5`, () => {
    generateRandoms();
    console.log('numeros randomicos gerados');
    console.log(`minutos entrada: ${minutosEntrada}`);
    console.log(`minutos almoço: ${minutosAlmoco}`);
    console.log(`minutos volta almoço: ${minutosVoltaAlmoco}`);
    console.log(`minutos saida: ${minutosSaida}`);
}, null, true, 'America/Sao_Paulo');


const cronFirstTime = new cronJob(`${minutosEntrada} 9 * * 1,2,3,4,5`, () => {
    pontoChegada();
    console.log('ponto chegada batido');
}, null, true, 'America/Sao_Paulo')

const cronSecondTime = new cronJob(`${minutosAlmoco} 12 * * 1,2,3,4,5`, () => {
    pontoAlmoco();
    console.log('ponto almoço batido');
}, null, true, 'America/Sao_Paulo')

const cronThirdTime = new cronJob(`${minutosVoltaAlmoco} 13 * * 1,2,3,4,5`, () => {
    pontoVoltaAlmoco();
    console.log('ponto volta almoço batido');
}, null, true, 'America/Sao_Paulo')

const json = {
  server: "ok",
  cron: "rodando"
}
app.get('/', (req, res) => {
  res.send(json);
})

app.listen(port, () => {
    console.log('server on');
});